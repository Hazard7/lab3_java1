package sets;
import java.util.function.Function;

public class FunctionalSet {
    public static<T> PurelyFunctionalSet<T> empty() {
        return x -> x == null;
    }
    public static <T> PurelyFunctionalSet<T> singletonSet(T value) {
        return x -> x == value;
    }
    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> set1, PurelyFunctionalSet<T> set2){
        return x -> set1.contains(x) || set2.contains(x);
    }
    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> set1, PurelyFunctionalSet<T> set2){
        return x -> set1.contains(x) && set2.contains(x);
    }
    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> set1, PurelyFunctionalSet<T> set2){
        return x -> set1.contains(x) && !set2.contains(x);
    }
    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> set, Predicate<T> p){
        return (x) -> p.test(x) && set.contains(x);
    }


    public static boolean forall(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return bypassCollection(s, p, -1000, 1000);
    }
    private static boolean bypassCollection(PurelyFunctionalSet<Integer> s, Predicate<Integer> p, Integer current, Integer maxBound) {
        if (current > maxBound)
            return true;

        if (s.contains(current) && !p.test(current))
            return false;

        //
        return bypassCollection(s, p, ++current, maxBound);
    }

    public static boolean exists(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return !forall(s, x -> !p.test(x));
    }

    public static <T> PurelyFunctionalSet<Integer> map(PurelyFunctionalSet<Integer> s,
                                                       Function<Integer, T > p) {
        return y -> exists(s, x -> p.apply(x) == y);
    }

    public static boolean existsOne(PurelyFunctionalSet<Integer> s,
                                 Predicate<Integer> p) {
        return !forall(s, x -> !p.test(x));
    }
}