package sets;


public interface PurelyFunctionalSet<T> {
    boolean contains(T element);
}
